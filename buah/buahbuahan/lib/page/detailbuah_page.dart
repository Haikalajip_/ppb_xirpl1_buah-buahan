import 'package:buahbuahan/page/webbuah_page.dart';
import 'package:flutter/material.dart';
import '../model/buah_model.dart';

class DetailBuahPage extends StatelessWidget {
  final BuahModel? buah;

  const DetailBuahPage({Key? key, this.buah}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text(buah!.namaBuah!),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => WebBuahPage(
                              webBuah: buah!.linkBuah!,
                            )));
              },
              icon: const Icon(Icons.open_in_browser))
        ],
      ),
      backgroundColor: Colors.purple,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              width: double.infinity,
              child: Image.asset(
                buah!.gambarBuah!,
                width: 100,
                height: 100,
                fit: BoxFit.fill,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                buah!.namaBuah!,
                style:
                    const TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                buah!.detailBuah!,
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 18),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
