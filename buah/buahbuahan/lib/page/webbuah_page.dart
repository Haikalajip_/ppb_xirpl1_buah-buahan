import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebBuahPage extends StatelessWidget {
  final String? webBuah;

  const WebBuahPage({Key? key, this.webBuah}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (webBuah == null) {
      // Handle kasus di mana webBuah adalah null
      return Scaffold(
        appBar: AppBar(
          title: const Text("Buah-Buahan"),
        ),
        body: const Center(
          child: Text("URL tidak valid atau tidak tersedia."),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Web Buah Page"), // Ganti judul sesuai kebutuhan Anda
      ),
      body: WebviewScaffold(
        url: webBuah!,
      ),
    );
  }
}
