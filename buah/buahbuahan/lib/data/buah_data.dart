import 'package:buahbuahan/model/buah_model.dart';

class BuahData {
  static var itemBuah = [
    BuahModel(
        namaBuah: "Anggur",
        gambarBuah: "assets/Anggur.png",
        detailBuah:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eleifend efficitur elit, eu tristique neque suscipit vel. Vestibulum quis sollicitudin libero. Vivamus accumsan quam id libero lacinia, a fringilla urna iaculis. Fusce hendrerit urna eu magna facilisis bibendum. Aenean id erat nec nulla cursus congue. Duis hendrerit id ex ut tincidunt. Maecenas ut nisi nec justo cursus dignissim",
        linkBuah: "https://id.wikipedia.org/wiki/Anggur"),
    BuahModel(
        namaBuah: "Mangga",
        gambarBuah: "assets/Mangga.png",
        detailBuah:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eleifend efficitur elit, eu tristique neque suscipit vel. Vestibulum quis sollicitudin libero. Vivamus accumsan quam id libero lacinia, a fringilla urna iaculis. Fusce hendrerit urna eu magna facilisis bibendum. Aenean id erat nec nulla cursus congue. Duis hendrerit id ex ut tincidunt. Maecenas ut nisi nec justo cursus dignissim",
        linkBuah: "https://id.wikipedia.org/wiki/Mangga"),
    BuahModel(
        namaBuah: "Cerry",
        gambarBuah: "assets/Cerry.png",
        detailBuah:
            " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eleifend efficitur elit, eu tristique neque suscipit vel. Vestibulum quis sollicitudin libero. Vivamus accumsan quam id libero lacinia, a fringilla urna iaculis. Fusce hendrerit urna eu magna facilisis bibendum. Aenean id erat nec nulla cursus congue. Duis hendrerit id ex ut tincidunt. Maecenas ut nisi nec justo cursus dignissim",
        linkBuah: "https://id.wikipedia.org/wiki/Mangga"),
    BuahModel(
        namaBuah: "Pisang",
        gambarBuah: "assets/Pisang.png",
        detailBuah:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eleifend efficitur elit, eu tristique neque suscipit vel. Vestibulum quis sollicitudin libero. Vivamus accumsan quam id libero lacinia, a fringilla urna iaculis. Fusce hendrerit urna eu magna facilisis bibendum. Aenean id erat nec nulla cursus congue. Duis hendrerit id ex ut tincidunt. Maecenas ut nisi nec justo cursus dignissim",
        linkBuah: "https://id.wikipedia.org/wiki/Pisang"),
    BuahModel(
        namaBuah: "Jeruk",
        gambarBuah: "assets/Jeruk.png",
        detailBuah:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eleifend efficitur elit, eu tristique neque suscipit vel. Vestibulum quis sollicitudin libero. Vivamus accumsan quam id libero lacinia, a fringilla urna iaculis. Fusce hendrerit urna eu magna facilisis bibendum. Aenean id erat nec nulla cursus congue. Duis hendrerit id ex ut tincidunt. Maecenas ut nisi nec justo cursus dignissim",
        linkBuah: "https://id.wikipedia.org/wiki/Jeruk"),
  ];

  //get all data
  static var itemCount = itemBuah.length;

  //get data by index
  static BuahModel? getItemBuah(int index) {
    return itemBuah[index];
  }
}
